package com.webencyclop.gitlab.test;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller
public class RestController {

  @Get("/")
  public String home() {
    return "working";
  }
}
