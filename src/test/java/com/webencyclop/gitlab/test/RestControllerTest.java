package com.webencyclop.gitlab.test;

import static org.junit.jupiter.api.Assertions.*;

import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@MicronautTest
class RestControllerTest {

  @Inject RestController restController;

  @Test
  void testEagerSingleton() {
    Assertions.assertEquals("working", restController.home());
  }
}
